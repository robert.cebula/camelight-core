package com.gitlab.robert.cebula.camelight_core;

import org.junit.Test;

import java.util.*;

import static com.gitlab.robert.cebula.camelight_core.BoardImpl.BOARD_SIZE;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

public class CamelStageProbabilitiesTest {
    private Camel orangeCamel;
    private Camel yellowCamel;
    private Camel whiteCamel;
    private Camel blueCamel;
    private Camel greenCamel;

    private void constructNewCamels() {
        orangeCamel = new Camel(CamelColor.ORANGE);
        yellowCamel = new Camel(CamelColor.YELLOW);
        whiteCamel = new Camel(CamelColor.WHITE);
        blueCamel = new Camel(CamelColor.BLUE);
        greenCamel = new Camel(CamelColor.GREEN);
    }

    @Test
    public void countProbabilitiesOneDiceLeftMetaAhead() {
        // given
        constructNewCamels();
        BoardImpl startingBoard = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>() {{
            put(12, new BoardElement[]{ yellowCamel });
            put(13, new BoardElement[]{ orangeCamel, blueCamel });
            put(14, new BoardElement[]{ whiteCamel, greenCamel });
            put(15, new BoardElement[]{ new Plate(-1) });
        }});
        List<CamelColor> leftCamelColors = new ArrayList<>(Collections.singletonList(CamelColor.ORANGE));
        CamelStageProbabilities camelStageProbabilities = new CamelStageProbabilities(startingBoard, leftCamelColors);

        // when
        StageProbabilities stageProbabilities = camelStageProbabilities.countProbabilities();

        // then
        Probabilities expectedWinners = new Probabilities(Arrays.asList(
                new Probability(CamelColor.ORANGE, 0.00f),
                new Probability(CamelColor.YELLOW, 0.0f),
                new Probability(CamelColor.GREEN, 0.33f),
                new Probability(CamelColor.BLUE, 0.66f),
                new Probability(CamelColor.WHITE, 0.00f)
        ));

        Probabilities expectedSecondPlaces = new Probabilities(Arrays.asList(
                new Probability(CamelColor.ORANGE, 0.66f),
                new Probability(CamelColor.YELLOW, 0.0f),
                new Probability(CamelColor.GREEN, 0.00f),
                new Probability(CamelColor.BLUE, 0.00f),
                new Probability(CamelColor.WHITE, 0.33f)
        ));

        int[] expectedSteps = new int[BOARD_SIZE];
        Arrays.fill(expectedSteps, 0);
        expectedSteps[14] = 1;
        expectedSteps[15] = 1;
        expectedSteps[0] = 1;

        assertThat(stageProbabilities.winnersProbabilities().getCopyOfProbabilities(),
                containsInAnyOrder(expectedWinners.getCopyOfProbabilities().toArray()));

        assertThat(stageProbabilities.secondPlacesProbabilities().getCopyOfProbabilities(),
                containsInAnyOrder(expectedSecondPlaces.getCopyOfProbabilities().toArray()));

        assertArrayEquals(expectedSteps, stageProbabilities.steps());
    }

    @Test
    public void countProbabilitiesTwoDicesLeftMetaAhead() {
        // given
        constructNewCamels();
        BoardImpl startingBoard = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>() {{
            put(9, new BoardElement[]{ yellowCamel });
            put(13, new BoardElement[]{ orangeCamel, blueCamel });
            put(14, new BoardElement[]{ whiteCamel, greenCamel });
            put(15, new BoardElement[]{ new Plate(1) });
        }});
        List<CamelColor> leftCamelColors = new ArrayList<>(Arrays.asList(CamelColor.ORANGE, CamelColor.BLUE));
        CamelStageProbabilities camelStageProbabilities = new CamelStageProbabilities(startingBoard, leftCamelColors);

        // when
        StageProbabilities stageProbabilities = camelStageProbabilities.countProbabilities();

        // then
        Probabilities expectedWinners = new Probabilities(Arrays.asList(
                new Probability(CamelColor.ORANGE, 0.16f),
                new Probability(CamelColor.YELLOW, 0.0f),
                new Probability(CamelColor.GREEN, 0.0f),
                new Probability(CamelColor.BLUE, 0.83f),
                new Probability(CamelColor.WHITE, 0.0f)
        ));

        Probabilities expectedSecondPlaces = new Probabilities(Arrays.asList(
                new Probability(CamelColor.ORANGE, 0.5f),
                new Probability(CamelColor.YELLOW, 0.0f),
                new Probability(CamelColor.GREEN, 0.33f),
                new Probability(CamelColor.BLUE, 0.16f),
                new Probability(CamelColor.WHITE, 0.0f)
        ));

        int[] expectedSteps = new int[BOARD_SIZE];
        Arrays.fill(expectedSteps, 0);
        expectedSteps[14] = 7;
        expectedSteps[15] = 8;
        expectedSteps[0] = 8;
        expectedSteps[1] = 1;

        assertThat(stageProbabilities.winnersProbabilities().getCopyOfProbabilities(),
                containsInAnyOrder(expectedWinners.getCopyOfProbabilities().toArray()));

        assertThat(stageProbabilities.secondPlacesProbabilities().getCopyOfProbabilities(),
                containsInAnyOrder(expectedSecondPlaces.getCopyOfProbabilities().toArray()));

        assertArrayEquals(expectedSteps, stageProbabilities.steps());
    }
}