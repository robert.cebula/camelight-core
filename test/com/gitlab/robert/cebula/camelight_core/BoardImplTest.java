package com.gitlab.robert.cebula.camelight_core;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class BoardImplTest {
    private Camel orangeCamel;
    private Camel yellowCamel;
    private Camel whiteCamel;
    private Camel blueCamel;
    private Camel greenCamel;

    private void constructNewCamels() {
        orangeCamel = new Camel(CamelColor.ORANGE);
        yellowCamel = new Camel(CamelColor.YELLOW);
        whiteCamel = new Camel(CamelColor.WHITE);
        blueCamel = new Camel(CamelColor.BLUE);
        greenCamel = new Camel(CamelColor.GREEN);
    }

    @Test
    public void testCopyBoard() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel, yellowCamel });
            put(3, new BoardElement[]{ new Plate(1) });
            put(4, new BoardElement[]{ blueCamel });
            put(5, new BoardElement[]{ whiteCamel, greenCamel });
        }});
        BoardImpl copiedBoard = new BoardImpl(board);
        assertEquals(board.toString(), copiedBoard.toString());

        // when
        copiedBoard.move(new Dice(CamelColor.ORANGE, 3));

        // then
        assertNotEquals(board.toString(), copiedBoard.toString());
    }

    @Test
    public void testMoveTwoCamelsOntoOtherTwo() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel, yellowCamel });
            put(3, new BoardElement[]{ new Plate(1) });
            put(4, new BoardElement[]{ blueCamel });
            put(5, new BoardElement[]{ whiteCamel, greenCamel });
        }});

        // when
        board.move(new Dice(CamelColor.ORANGE, 3));

        // then
        constructNewCamels();
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(3, new BoardElement[]{ new Plate(1) });
            put(4, new BoardElement[]{ blueCamel });
            put(5, new BoardElement[]{ whiteCamel, greenCamel, orangeCamel, yellowCamel });
        }});

        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testMoveCamelsOntoPlatePlusOneAdjucentToCamel() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel, yellowCamel });
            put(3, new BoardElement[]{ new Plate(1) });
            put(4, new BoardElement[]{ blueCamel });
            put(5, new BoardElement[]{ whiteCamel, greenCamel });
        }});

        // when
        board.move(new Dice(CamelColor.YELLOW, 1));

        // then
        constructNewCamels();
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel });
            put(3, new BoardElement[]{ new Plate(1) });
            put(4, new BoardElement[]{ blueCamel, yellowCamel });
            put(5, new BoardElement[]{ whiteCamel, greenCamel });
        }});

        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testMoveCamelsOntoPlatePlusOneAdjucentToEmptyField() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel, yellowCamel });
            put(3, new BoardElement[]{ new Plate(1) });
            put(4, new BoardElement[]{ new EmptyBoardElement() });
            put(5, new BoardElement[]{ whiteCamel, greenCamel, blueCamel });
        }});

        // when
        board.move(new Dice(CamelColor.YELLOW, 1));

        // then
        constructNewCamels();
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel });
            put(3, new BoardElement[]{ new Plate(1) });
            put(4, new BoardElement[]{ yellowCamel });
            put(5, new BoardElement[]{ whiteCamel, greenCamel, blueCamel});
        }});

        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testMoveCamelsOntoPlateMinusOneAdjucentToCamel() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel, yellowCamel, whiteCamel });
            put(4, new BoardElement[]{ greenCamel, blueCamel });
            put(5, new BoardElement[]{ new Plate(-1) });
        }});

        // when
        board.move(new Dice(CamelColor.YELLOW, 3));

        // then
        constructNewCamels();
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel });
            put(4, new BoardElement[]{ yellowCamel, whiteCamel, greenCamel, blueCamel});
            put(5, new BoardElement[]{ new Plate(-1) });
        }});

        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testMoveCamelsOntoPlateMinusOneAdjucentToEmptyField() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ orangeCamel, yellowCamel, whiteCamel, blueCamel, greenCamel });
            put(4, new BoardElement[]{ new Plate(-1) });
        }});

        // when
        board.move(new Dice(CamelColor.ORANGE, 2));

        // then
        constructNewCamels();
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(3, new BoardElement[]{ orangeCamel, yellowCamel, whiteCamel, blueCamel, greenCamel });
            put(4, new BoardElement[]{ new Plate(-1) });
        }});

        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testGetCamelResults() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(2, new BoardElement[]{ yellowCamel, whiteCamel });
            put(4, new BoardElement[]{ blueCamel, greenCamel });
            put(6, new BoardElement[]{ new Plate(-1) });
            put(9, new BoardElement[]{ orangeCamel });
        }});

        // when
        BoardImpl.CamelResults camelResults = board.getCamelResults();

        // then
        BoardImpl.CamelResults expected = new BoardImpl.CamelResults(CamelColor.YELLOW, CamelColor.WHITE, CamelColor.BLUE,
                CamelColor.GREEN, CamelColor.ORANGE);
        assertEquals(camelResults.toString(), expected.toString());
        assertEquals(camelResults.getWinner(), CamelColor.ORANGE);
    }

    @Test
    public void testRemovePlates() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(7, new BoardElement[]{ new Plate(1) });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
            put(9, new BoardElement[]{ new Plate(-1) });
        }});

        // when
        board.removePlates();

        // then
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
        }});

        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testGetPlateAtPosition() {
        //given
        constructNewCamels();
        Plate plate = new Plate(1);
        Board board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(7, new BoardElement[]{ plate });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
            put(9, new BoardElement[]{ new Plate(-1) });
        }});

        // when
        Optional<Plate> plate1 = board.getPlateAtPosition(5);
        Optional<Plate> plate2 = board.getPlateAtPosition(7);

        // then
        assertFalse(plate1.isPresent());
        //noinspection OptionalGetWithoutIsPresent
        assertEquals(plate2.get(), plate);
    }

    @Test
    public void testRemovePlateAtPosiiton() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(7, new BoardElement[]{ new Plate(1) });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
            put(9, new BoardElement[]{ new Plate(-1) });
        }});

        // when
        board.removePlateAtPosition(7);

        // then
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
            put(9, new BoardElement[]{ new Plate(-1) });
        }});

        assertEquals(board.toString(), expected.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemovePlateAtPosition_whenNoPlate_shouldThrowException() {
        // given
        constructNewCamels();
        Board board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(7, new BoardElement[]{ new Plate(1) });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
            put(9, new BoardElement[]{ new Plate(-1) });
        }});

        // when
        board.removePlateAtPosition(5);
    }

    @Test
    public void testAddOrSwitchPlateAtPosition() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(7, new BoardElement[]{ new Plate(1) });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
        }});

        // when
        boolean firstAction = board.addOrSwitchPlateAtPosition(new Plate(-1), 7);
        boolean secondAction = board.addOrSwitchPlateAtPosition(new Plate(-1), 0);
        boolean thirdAction = board.addOrSwitchPlateAtPosition(new Plate(1), 5);
        boolean fourthAction = board.addOrSwitchPlateAtPosition(null, 11);
        boolean fifthAction = board.addOrSwitchPlateAtPosition(new Plate(1), 4);

        // then
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(4, new BoardElement[]{ new Plate(1) });
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(7, new BoardElement[]{ new Plate(-1) });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
        }});

        assertTrue(firstAction);
        assertFalse(secondAction);
        assertFalse(thirdAction);
        assertTrue(fourthAction);
        assertTrue(fifthAction);
        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testConstructBoardFromDices() {
        // given
        List<Dice> dices = new ArrayList<Dice>() {{
            add(new Dice(CamelColor.BLUE, 2));
            add(new Dice(CamelColor.ORANGE, 1));
            add(new Dice(CamelColor.WHITE, 2));
            add(new Dice(CamelColor.GREEN, 3));
            add(new Dice(CamelColor.YELLOW, 3));
        }};

        // when
        BoardImpl board = BoardImpl.constructBoard(dices);

        // then
        constructNewCamels();
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(0, new BoardElement[]{ orangeCamel });
            put(1, new BoardElement[]{ blueCamel, whiteCamel });
            put(2, new BoardElement[]{ greenCamel, yellowCamel });
        }});

        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testTryToMoveUpperCamel() {
        // given
        constructNewCamels();
        BoardImpl board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(7, new BoardElement[]{ new Plate(1) });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
        }});

        // when
        boolean firstAction = board.tryToMoveUpperCamel(5, 10);
        boolean secondAction = board.tryToMoveUpperCamel(10, 12);
        boolean thirdAction = board.tryToMoveUpperCamel(8, 8);
        boolean fourthAction = board.tryToMoveUpperCamel(5, 7);
        boolean fifthAction = board.tryToMoveUpperCamel(5, 12);
        boolean sixthAction = board.tryToMoveUpperCamel(5, 6);

        // then
        BoardImpl expected = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(6, new BoardElement[]{ yellowCamel });
            put(7, new BoardElement[]{ new Plate(1) });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
            put(12, new BoardElement[]{ blueCamel, orangeCamel });
        }});

        assertTrue(firstAction);
        assertTrue(secondAction);
        assertFalse(thirdAction);
        assertFalse(fourthAction);
        assertTrue(fifthAction);
        assertTrue(sixthAction);
        assertEquals(board.toString(), expected.toString());
    }

    @Test
    public void testContainsPlates() {
        // given
        constructNewCamels();
        Board board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(7, new BoardElement[]{ new Plate(1) });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
            put(9, new BoardElement[]{ new Plate(-1) });
        }});

        // when
        boolean result = board.containsPlates();

        // then
        assertTrue(result);
    }

    @Test
    public void testContainsPlates_whenNoPlates_shouldReturnFalse() {
        // given
        constructNewCamels();
        Board board = BoardImpl.constructBoard(new HashMap<Integer, BoardElement[]>(){{
            put(5, new BoardElement[]{ yellowCamel, orangeCamel, blueCamel });
            put(8, new BoardElement[]{ whiteCamel, greenCamel });
        }});

        // when
        boolean result = board.containsPlates();

        // then
        assertFalse(result);
    }
}