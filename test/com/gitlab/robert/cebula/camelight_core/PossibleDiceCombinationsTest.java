package com.gitlab.robert.cebula.camelight_core;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

public class PossibleDiceCombinationsTest {
    @Test
    public void testPossibleCombinationsForTwoDices() {
        // given
        PossibleDiceCombinations possibleDiceCombinations =
                new PossibleDiceCombinations(Arrays.asList(CamelColor.YELLOW, CamelColor.ORANGE));

        // when
        List<PossibleDiceCombinations.DiceCombination> diceCombinations = new ArrayList<>();
        for (PossibleDiceCombinations.DiceCombination diceCombination : possibleDiceCombinations) {
            diceCombinations.add(diceCombination);
        }

        // then
        List<PossibleDiceCombinations.DiceCombination> expected = new ArrayList<PossibleDiceCombinations.DiceCombination>() {{
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 1), new Dice(CamelColor.ORANGE, 1)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 1), new Dice(CamelColor.ORANGE, 2)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 1), new Dice(CamelColor.ORANGE, 3)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 2), new Dice(CamelColor.ORANGE, 1)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 2), new Dice(CamelColor.ORANGE, 2)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 2), new Dice(CamelColor.ORANGE, 3)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 3), new Dice(CamelColor.ORANGE, 1)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 3), new Dice(CamelColor.ORANGE, 2)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.YELLOW, 3), new Dice(CamelColor.ORANGE, 3)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 1), new Dice(CamelColor.YELLOW, 1)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 1), new Dice(CamelColor.YELLOW, 2)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 1), new Dice(CamelColor.YELLOW, 3)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 2), new Dice(CamelColor.YELLOW, 1)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 2), new Dice(CamelColor.YELLOW, 2)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 2), new Dice(CamelColor.YELLOW, 3)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 3), new Dice(CamelColor.YELLOW, 1)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 3), new Dice(CamelColor.YELLOW, 2)));
            add(new PossibleDiceCombinations.DiceCombination(new Dice(CamelColor.ORANGE, 3), new Dice(CamelColor.YELLOW, 3)));
        }};

        assertThat(diceCombinations, containsInAnyOrder(expected.toArray()));
    }

    private int factorial(int n) {
        int fact = 1;
        for (int i = 2; i <= n; i++) {
            fact = fact * i;
        }
        return fact;
    }

    private int power(int a, int b) {
        int result = 1;
        for (int it = 0; it < b; ++it) {
            result *= a;
        }

        return result;
    }

    @Test
    public void testPossibleCombinationsForThreeDices_compareSizes() {
        // given
        PossibleDiceCombinations possibleDiceCombinations =
                new PossibleDiceCombinations(Arrays.asList(CamelColor.YELLOW, CamelColor.ORANGE, CamelColor.WHITE));

        // when
        List<PossibleDiceCombinations.DiceCombination> diceCombinations = new ArrayList<>();
        for (PossibleDiceCombinations.DiceCombination diceCombination : possibleDiceCombinations) {
            diceCombinations.add(diceCombination);
        }

        // then
        int expectedSize = power(3, 3) * factorial(3);

        assertEquals(expectedSize, diceCombinations.size());
    }

    @Test
    public void testPossibleCombinationsForFourDices_compareSizes() {
        // given
        PossibleDiceCombinations possibleDiceCombinations =
                new PossibleDiceCombinations(Arrays.asList(CamelColor.YELLOW, CamelColor.ORANGE, CamelColor.WHITE,
                        CamelColor.BLUE));

        // when
        List<PossibleDiceCombinations.DiceCombination> diceCombinations = new ArrayList<>();
        for (PossibleDiceCombinations.DiceCombination diceCombination : possibleDiceCombinations) {
            diceCombinations.add(diceCombination);
        }

        // then
        int expectedSize = power(3, 4) * factorial(4);

        assertEquals(expectedSize, diceCombinations.size());
    }

    @Test
    public void testPossibleCombinationsForFiveDices_compareSizes() {
        // given
        PossibleDiceCombinations possibleDiceCombinations =
                new PossibleDiceCombinations(Arrays.asList(CamelColor.YELLOW, CamelColor.ORANGE, CamelColor.WHITE,
                        CamelColor.BLUE, CamelColor.GREEN));

        // when
        List<PossibleDiceCombinations.DiceCombination> diceCombinations = new ArrayList<>();
        for (PossibleDiceCombinations.DiceCombination diceCombination : possibleDiceCombinations) {
            diceCombinations.add(diceCombination);
        }

        // then
        int expectedSize = power(3, 5) * factorial(5);

        assertEquals(expectedSize, diceCombinations.size());
    }
}
