package com.gitlab.robert.cebula.camelight_core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Test;

public class CamelTest {
    @Test
    public void testThisAndUpperCamelsColors() {
        // given
        Camel orangeCamel = new Camel(CamelColor.ORANGE);
        Camel yellowCamel = new Camel(CamelColor.YELLOW);
        Camel greenCamel = new Camel(CamelColor.GREEN);

        orangeCamel.setUpCamel(yellowCamel);
        yellowCamel.setUpCamel(greenCamel);

        // when
        List<CamelColor> camelColors = orangeCamel.thisAndUpperCamelsColors();

        // then
        List<CamelColor> expected = new ArrayList<CamelColor>() {{
           add(CamelColor.ORANGE);
           add(CamelColor.YELLOW);
           add(CamelColor.GREEN);
        }};

        assertThat(camelColors,
                IsIterableContainingInOrder.contains(expected.toArray()));
    }
}