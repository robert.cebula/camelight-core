package com.gitlab.robert.cebula.camelight_core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StageBoardTest {
    @Mock
    private BoardImpl boardImpl;

    @Test
    public void testMove() {
        // given
        List<CamelColor> leftColors = new ArrayList<>(Arrays.asList(CamelColor.BLUE, CamelColor.GREEN, CamelColor.ORANGE));
        StageBoard stageBoard = new StageBoard(boardImpl, leftColors);

        // when
        stageBoard.move(new Dice(CamelColor.BLUE, 2));

        // then
        List<CamelColor> expectedColors = Arrays.asList(CamelColor.GREEN, CamelColor.ORANGE);
        assertThat(stageBoard.leftColors(), containsInAnyOrder(expectedColors.toArray()));

        List<Dice> expectedDices = Collections.singletonList(new Dice(CamelColor.BLUE, 2));
        assertThat(stageBoard.usedDices(), containsInAnyOrder(expectedDices.toArray()));
    }

    @Test
    public void testMove_whenOneDiceLeft_shouldRefillAndRemovePlates() {
        // given
        List<CamelColor> leftColors = new ArrayList<>(Collections.singletonList(CamelColor.BLUE));
        StageBoard stageBoard = new StageBoard(boardImpl, leftColors);

        // when
        stageBoard.move(new Dice(CamelColor.BLUE, 2));

        // then
        List<CamelColor> expectedColors = Arrays.asList(CamelColor.values());
        assertThat(stageBoard.leftColors(), containsInAnyOrder(expectedColors.toArray()));

        assertTrue(stageBoard.usedDices().isEmpty());

        verify(boardImpl).removePlates();
    }
}