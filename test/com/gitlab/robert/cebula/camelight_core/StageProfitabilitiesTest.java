package com.gitlab.robert.cebula.camelight_core;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class StageProfitabilitiesTest {
    @Test
    public void testStageProfitabilities() {
        // given
        Map<CamelColor, Integer> numberOfWinsByCamelColor = new HashMap<CamelColor, Integer>() {{
            put(CamelColor.ORANGE, 5);
            put(CamelColor.BLUE, 3);
            put(CamelColor.GREEN, 2);
            put(CamelColor.YELLOW, 0);
            put(CamelColor.WHITE, 0);
        }};
        Map<CamelColor, Integer> numberOfSecondPlacesByCamelColor = new HashMap<CamelColor, Integer>() {{
            put(CamelColor.ORANGE, 4);
            put(CamelColor.BLUE, 1);
            put(CamelColor.GREEN, 2);
            put(CamelColor.YELLOW, 3);
            put(CamelColor.WHITE, 0);
        }};

        StageProbabilities stageProbabilities = new StageProbabilities(numberOfWinsByCamelColor,
                numberOfSecondPlacesByCamelColor, new int[0]);

        // when
        StageProfitabilities stageProfitabilities = new StageProfitabilities(stageProbabilities);
        List<Profitability> profitabilities = stageProfitabilities.profitabilities();

        // then
        List<Profitability> expected = new ArrayList<Profitability>() {{
            add(new Profitability(CamelColor.ORANGE, 2.8f, 1.8f, 1.3f));
            add(new Profitability(CamelColor.BLUE, 1.0f, 0.4f, 0.1f));
            add(new Profitability(CamelColor.GREEN, 0.6f, 0.2f, 0.0f));
            add(new Profitability(CamelColor.YELLOW, -0.4f, -0.4f, -0.4f));
            add(new Profitability(CamelColor.WHITE, -1.0f, -1.0f, -1.0f));
        }};
        assertThat(profitabilities, containsInAnyOrder(expected.toArray()));
    }
}