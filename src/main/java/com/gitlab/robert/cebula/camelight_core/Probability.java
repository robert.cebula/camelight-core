package com.gitlab.robert.cebula.camelight_core;

public class Probability implements Comparable<Probability> {
    private final CamelColor camelColor;
    private final float probability;

    Probability(CamelColor camelColor, float probability) {
        this.camelColor = camelColor;
        this.probability = probability;
    }

    public CamelColor camelColor() {
        return this.camelColor;
    }

    public float probability() {
        return this.probability;
    }

    @Override
    public int compareTo(Probability o) {
        return Float.compare(this.probability, o.probability);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Probability)) {
            return false;
        }

        Probability other = (Probability) obj;

        float epsilon = 0.01f;
        return this.camelColor.equals(other.camelColor) && Math.abs(this.probability - other.probability) < epsilon;
    }

    @Override
    public String toString() {
        return this.camelColor.toString() + ": " + this.probability;
    }
}
