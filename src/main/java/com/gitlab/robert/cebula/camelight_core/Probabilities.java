package com.gitlab.robert.cebula.camelight_core;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Probabilities implements Iterable<Probability>{
    private List<Probability> probabilities;

    Probabilities(List<Probability> probabilities) {
        this.probabilities = probabilities;
    }

    Probabilities(Map<CamelColor, Integer> numberOfWinsOrLoosesByCamelColor) {
        this.probabilities = new ArrayList<>();

        int totalNumberOfWinsOrLooses = 0;
        for (Integer numberOfWinsOrLooses : numberOfWinsOrLoosesByCamelColor.values()) {
            totalNumberOfWinsOrLooses += numberOfWinsOrLooses;
        }

        for (CamelColor camelColor : numberOfWinsOrLoosesByCamelColor.keySet()) {
            int numberOfWinsOrLooses = numberOfWinsOrLoosesByCamelColor.get(camelColor);
            float percentageOfWinsOrLooses = (float)numberOfWinsOrLooses / (float)totalNumberOfWinsOrLooses;
            this.probabilities.add(new Probability(camelColor, percentageOfWinsOrLooses));
        }
    }

    @Override
    @NonNull
    public Iterator<Probability> iterator() {
        return probabilities.iterator();
    }

    public List<Probability> getCopyOfProbabilities() {
        return new ArrayList<>(this.probabilities);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        for (Probability probability : this.probabilities) {
            result.append(probability.toString()).append("\n");
        }

        return result.toString();
    }
}
