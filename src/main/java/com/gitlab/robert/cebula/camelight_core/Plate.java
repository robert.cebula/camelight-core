package com.gitlab.robert.cebula.camelight_core;

public class Plate implements BoardElement {
    private final int platePositionChange;

    public Plate(int platePositionChange) {
        this.platePositionChange = platePositionChange;
    }

    public int platePositionChange() {
        return platePositionChange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Plate plate = (Plate) o;

        return platePositionChange == plate.platePositionChange;
    }

    @Override
    public int hashCode() {
        return platePositionChange;
    }

    void action(Camel camel, int position, BoardImpl board) {
        camel.move(platePositionChange, board, position);
    }

    public BoardElement copy() {
        return new Plate(this.platePositionChange);
    }

    @Override
    public String toString() {
        String sign = platePositionChange > 0 ? "+" : "-";
        return sign + platePositionChange;
    }
}
