package com.gitlab.robert.cebula.camelight_core;

public enum CamelColor {
    ORANGE,
    BLUE,
    WHITE,
    YELLOW,
    GREEN;
}
