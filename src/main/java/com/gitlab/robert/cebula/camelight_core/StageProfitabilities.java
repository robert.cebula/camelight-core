package com.gitlab.robert.cebula.camelight_core;

import java.util.*;

public class StageProfitabilities implements Iterable<Profitability> {
    private final List<Profitability> profitabilities;

    public StageProfitabilities(StageProbabilities stageProbabilities) {
        this.profitabilities = countProfitabilities(getProbabilities(stageProbabilities));
    }

    public List<Profitability> profitabilities() {
        return profitabilities;
    }

    @Override
    public Iterator<Profitability> iterator() {
        return this.profitabilities.iterator();
    }

    private Map<CamelColor, CamelFirstAndSecondPlaceProbability> getProbabilities(StageProbabilities stageProbabilities) {
        Probabilities winnersProbabilities = stageProbabilities.winnersProbabilities();
        Probabilities secondPlacesProbabilities = stageProbabilities.secondPlacesProbabilities();

        Map<CamelColor, CamelFirstAndSecondPlaceProbability> result = new HashMap<>();

        for (Probability winnerProbability : winnersProbabilities) {
            CamelColor winnerColor = winnerProbability.camelColor();
            for (Probability secondPlaceProbability : secondPlacesProbabilities) {
                if (secondPlaceProbability.camelColor().equals(winnerColor)) {
                    CamelFirstAndSecondPlaceProbability camelFirstAndSecondPlaceProbability =
                            new CamelFirstAndSecondPlaceProbability(
                                    winnerProbability.probability(),
                                    secondPlaceProbability.probability()
                            );
                    result.put(winnerColor, camelFirstAndSecondPlaceProbability);
                    break;
                }
            }
        }

        return result;
    }

    private List<Profitability> countProfitabilities(Map<CamelColor, CamelFirstAndSecondPlaceProbability> probabilities) {
        List<Profitability> result = new ArrayList<>();

        for (CamelColor camelColor : probabilities.keySet()) {
            CamelFirstAndSecondPlaceProbability camelFirstAndSecondPlaceProbability = probabilities.get(camelColor);
            Profitability profitability = new Profitability(
                    camelColor,
                    camelFirstAndSecondPlaceProbability.firstPlaceProbability,
                    camelFirstAndSecondPlaceProbability.secondPlaceProbability
            );
            result.add(profitability);
        }

        return result;
    }

    private static class CamelFirstAndSecondPlaceProbability {
        private final float firstPlaceProbability;
        private final float secondPlaceProbability;

        CamelFirstAndSecondPlaceProbability(float firstPlaceProbability, float secondPlaceProbability) {
            this.firstPlaceProbability = firstPlaceProbability;
            this.secondPlaceProbability = secondPlaceProbability;
        }

        float firstPlaceProbability() {
            return firstPlaceProbability;
        }

        float secondPlaceProbability() {
            return secondPlaceProbability;
        }
    }
}