package com.gitlab.robert.cebula.camelight_core;

public class EmptyBoardElement implements BoardElement {
    public BoardElement copy() {
        return this;
    }

    @Override
    public String toString() {
        return "  ";
    }
}
