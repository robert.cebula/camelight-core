package com.gitlab.robert.cebula.camelight_core;

public interface Board {
    boolean removePlates();

    Optional<Plate> getPlateAtPosition(int position);

    boolean addOrSwitchPlateAtPosition(Plate newPlate, int position);

    void removePlateAtPosition(int position);

    void move(Dice dice);

    boolean tryToMoveUpperCamel(int startingPosition, int endingPosition);

    GameState currentGameState();

    BoardElement getBoardElement(int position);

    boolean containsPlates();
}
