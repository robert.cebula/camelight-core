package com.gitlab.robert.cebula.camelight_core;

import java.io.Serializable;

public interface BoardElement extends Serializable {
    BoardElement copy();
}
