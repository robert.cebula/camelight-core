package com.gitlab.robert.cebula.camelight_core;

public class Profitability {
    private final CamelColor camelColor;
    private final float fiveCoinsPlateProfitability;
    private final float threeCoinsPlateProfitability;
    private final float twoCoinsPlateProfitability;

    @SuppressWarnings("FieldCanBeLocal")
    private final float secondPlaceWin = 1.0f;
    @SuppressWarnings("FieldCanBeLocal")
    private final float otherPlacesLoose = 1.0f;

    Profitability(CamelColor camelColor, float firstPlaceProbability, float secondPlaceProbability) {
        this.camelColor = camelColor;

        float otherPlacesProbability = 1.0f - (firstPlaceProbability + secondPlaceProbability);

        fiveCoinsPlateProfitability = countProfitability(firstPlaceProbability,
                secondPlaceProbability, otherPlacesProbability, 5.0f);
        threeCoinsPlateProfitability = countProfitability(firstPlaceProbability,
                secondPlaceProbability, otherPlacesProbability, 3.0f);
        twoCoinsPlateProfitability = countProfitability(firstPlaceProbability,
                secondPlaceProbability, otherPlacesProbability, 2.0f);
    }

    public Profitability(CamelColor camelColor, float fiveCoinsPlateProfitability, float threeCoinsPlateProfitability,
                         float twoCoinsPlateProfitability) {
        this.camelColor = camelColor;
        this.fiveCoinsPlateProfitability = fiveCoinsPlateProfitability;
        this.threeCoinsPlateProfitability = threeCoinsPlateProfitability;
        this.twoCoinsPlateProfitability = twoCoinsPlateProfitability;
    }

    private float countProfitability(float firstPlaceProbability, float secondPlaceProbability,
                                     float otherPlacesProbability, float firstPlaceWin) {
        return (firstPlaceProbability * firstPlaceWin)
                + (secondPlaceProbability * secondPlaceWin)
                - (otherPlacesProbability * otherPlacesLoose);
    }

    public float fiveCoinsPlateProfitability() {
        return fiveCoinsPlateProfitability;
    }

    public float threeCoinsPlateProfitability() {
        return threeCoinsPlateProfitability;
    }

    public float twoCoinsPlateProfitability() {
        return twoCoinsPlateProfitability;
    }

    public CamelColor camelColor() {
        return camelColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profitability other = (Profitability) o;

        float epsilon = 0.01f;
        return this.camelColor.equals(other.camelColor) &&
                Math.abs(this.fiveCoinsPlateProfitability- other.fiveCoinsPlateProfitability) < epsilon &&
                Math.abs(this.threeCoinsPlateProfitability- other.threeCoinsPlateProfitability) < epsilon &&
                Math.abs(this.twoCoinsPlateProfitability- other.twoCoinsPlateProfitability) < epsilon;
    }
}
