package com.gitlab.robert.cebula.camelight_core;

import java.util.ArrayList;
import java.util.List;

import static com.gitlab.robert.cebula.camelight_core.BoardImpl.BOARD_SIZE;

public class Camel implements BoardElement {
    private final CamelColor camelColor;
    private boolean wentThroughFinish;
    private Camel upCamel = null;
    private Camel bottomCamel = null;

    Camel(CamelColor camelColor) {
        this(camelColor, false);
    }

    private Camel(CamelColor camelColor, boolean wentThroughFinish) {
        this.camelColor = camelColor;
        this.wentThroughFinish = wentThroughFinish;
    }

    private void goThroughFinish() {
        wentThroughFinish = true;
        if (upCamel != null) {
            upCamel.goThroughFinish();
        }
    }

    boolean wentThroughFinish() {
        return wentThroughFinish;
    }

    boolean itOrUpperCamelsWentThroughFinish() {
        Camel camel = this;
        while (camel != null) {
            if (camel.wentThroughFinish) {
                return true;
            }

            camel = camel.upCamel;
        }

        return false;
    }

    void move(int positionChange, BoardImpl board, int currentPosition) {
        if (bottomCamel == null) {
            board.removeCamelIfExist(currentPosition);
        }
        else {
            bottomCamel.upCamel = null;
            bottomCamel = null;
        }

        int newPosition = currentPosition + positionChange;
        if (newPosition >= BOARD_SIZE) {
            goThroughFinish();
            newPosition = newPosition % BOARD_SIZE;
        }

        BoardElement boardElementAtNewPosition = board.getBoardElement(newPosition);
        if (boardElementAtNewPosition instanceof Camel) {
            Camel camelAtNewPosition = (Camel)boardElementAtNewPosition;
            Direction direction = (positionChange > 0) ? Direction.FORWARD : Direction.BACKWARD;
            camelAtNewPosition.anotherCamelStepOnto(direction, this, board, newPosition);
            return;
        }
        if (boardElementAtNewPosition instanceof Plate) {
            Plate plateAtNewPosition = (Plate) boardElementAtNewPosition;
            plateAtNewPosition.action(this, newPosition, board);
            return;
        }
        if (boardElementAtNewPosition instanceof EmptyBoardElement) {
            board.setBoardElement(this, newPosition);
            return;
        }

        throw new RuntimeException("Unknown BoardElement " + boardElementAtNewPosition.toString());
    }

    public List<CamelColor> thisAndUpperCamelsColors() {
        List<CamelColor> camelColors = new ArrayList<>();

        Camel camel = this;
        while (camel != null) {
            camelColors.add(camel.camelColor);
            camel = camel.upCamel;
        }

        return camelColors;
    }

    private void anotherCamelStepOnto(Direction direction, Camel otherCamel, BoardImpl board, int position) {
        if (direction.equals(Direction.FORWARD)) {
            Camel mostUpperCamel = mostUpperCamel();
            mostUpperCamel.upCamel = otherCamel;
            otherCamel.bottomCamel = mostUpperCamel;
            return;
        }

        if (direction.equals(Direction.BACKWARD)) {
            board.setBoardElement(otherCamel, position);
            Camel mostUpperCamelOnOther = otherCamel.mostUpperCamel();
            mostUpperCamelOnOther.upCamel = this;
            this.bottomCamel = mostUpperCamelOnOther;
            return;
        }

        throw new RuntimeException("Unknown direction " + direction.toString());
    }

    public Camel mostUpperCamel() {
        if (upCamel == null) {
            return this;
        }

        return upCamel.mostUpperCamel();
    }

    boolean isColor(CamelColor camelColor) {
        return this.camelColor.equals(camelColor);
    }

    public BoardElement copy() {
        Camel newCamel = new Camel(this.camelColor, this.wentThroughFinish);
        if (upCamel != null) {
            Camel newUpCamel = (Camel) upCamel.copy();
            newUpCamel.bottomCamel = newCamel;
            newCamel.upCamel = newUpCamel;
        }

        return newCamel;
    }

    void setUpCamel(Camel upCamel) {
        this.upCamel = upCamel;
    }

    void setBottomCamel(Camel bottomCamel) {
        this.bottomCamel = bottomCamel;
    }

    Camel getUpCamel() {
        return upCamel;
    }

    Camel getBottomCamel() {
        return bottomCamel;
    }

    @Override
    public String toString() {
        return camelColor.toString().substring(0, 1) + " ";
    }

    public CamelColor color() {
        return camelColor;
    }

    private enum Direction {
        BACKWARD,
        FORWARD
    }
}
