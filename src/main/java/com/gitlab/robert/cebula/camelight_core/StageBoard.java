package com.gitlab.robert.cebula.camelight_core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StageBoard implements Board, Serializable {
    private final BoardImpl boardImpl;
    private final List<CamelColor> leftColors;
    private final List<Dice> usedDices;

    public StageBoard(StageBoard other) {
        this.boardImpl = new BoardImpl(other.boardImpl);
        this.leftColors = new ArrayList<>(other.leftColors);
        this.usedDices = new ArrayList<>(other.usedDices);
    }

    public StageBoard(BoardImpl boardImpl, List<CamelColor> leftColors) {
        this.boardImpl = boardImpl;
        this.leftColors = leftColors;
        this.usedDices = new ArrayList<>();
    }

    public List<Dice> usedDices() {
        return this.usedDices;
    }

    public List<CamelColor> leftColors() {
        return this.leftColors;
    }

    public BoardImpl boardImpl() {
        return boardImpl;
    }

    @Override
    public boolean containsPlates() {
        return this.boardImpl.containsPlates();
    }

    @Override
    public boolean removePlates() {
        return this.boardImpl.removePlates();
    }

    @Override
    public Optional<Plate> getPlateAtPosition(int position) {
        return this.boardImpl.getPlateAtPosition(position);
    }

    @Override
    public boolean addOrSwitchPlateAtPosition(Plate newPlate, int position) {
        return this.boardImpl.addOrSwitchPlateAtPosition(newPlate, position);
    }

    @Override
    public void removePlateAtPosition(int position) {
        this.boardImpl.removePlateAtPosition(position);
    }

    @Override
    public void move(Dice dice) {
        this.boardImpl.move(dice);
        this.leftColors.remove(dice.camelColor());
        this.usedDices.add(dice);

        if (this.leftColors.isEmpty()) {
            this.removePlates();
            this.leftColors.addAll(Arrays.asList(CamelColor.values()));
            this.usedDices.clear();
        }
    }

    @Override
    public boolean tryToMoveUpperCamel(int startingPosition, int endingPosition) {
        return this.boardImpl.tryToMoveUpperCamel(startingPosition, endingPosition);
    }

    @Override
    public GameState currentGameState() {
        return this.boardImpl.currentGameState();
    }

    @Override
    public BoardElement getBoardElement(int position) {
        return this.boardImpl.getBoardElement(position);
    }
}
