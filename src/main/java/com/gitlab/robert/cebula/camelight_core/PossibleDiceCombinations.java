package com.gitlab.robert.cebula.camelight_core;

import com.google.common.collect.Collections2;

import javax.annotation.Nonnull;
import java.util.*;

public class PossibleDiceCombinations implements Iterable<PossibleDiceCombinations.DiceCombination> {
    private final Set<CamelColor> availableColors;

    private final List<DiceCombination> diceCombinations = new ArrayList<>();

    PossibleDiceCombinations(List<CamelColor> camelColors) {
        availableColors = new HashSet<>();
        availableColors.addAll(camelColors);

        generateDiceCombinations();
    }

    private Collection<List<Integer>> possibleDiceValues(int numberOfDices) {
        Integer[] dices = new Integer[numberOfDices];
        for (int it = 0; it < dices.length; ++it) {
            dices[it] = 1;
        }

        Collection<List<Integer>> dicesCollection = new ArrayList<>();

        for (int outerIt = 0; outerIt < Math.pow(Dice.MAX_VALUE, numberOfDices); ++outerIt) {
            dicesCollection.add(new ArrayList<>(Arrays.asList(dices)));

            for (int innerIt = 0; innerIt < numberOfDices; ++innerIt) {
                if (dices[innerIt] < Dice.MAX_VALUE) {
                    dices[innerIt]++;
                    break;
                } else {
                    dices[innerIt] = 1;
                }
            }
        }

        return dicesCollection;
    }

    private void generateDiceCombinations() {
        @SuppressWarnings("UnstableApiUsage") Collection<List<CamelColor>> possibleColors =
                Collections2.permutations(availableColors);
        Collection<List<Integer>> possibleDiceValues = possibleDiceValues(availableColors.size());

        for (List<CamelColor> camelColors : possibleColors) {
            for (List<Integer> diceValues : possibleDiceValues) {
                List<Dice> dices = new ArrayList<>();
                for (int it = 0; it < camelColors.size(); ++it) {
                    dices.add(new Dice(camelColors.get(it), diceValues.get(it)));
                }
                diceCombinations.add(new DiceCombination(dices));
            }
        }
    }

    @Override
    @Nonnull
    public Iterator<DiceCombination> iterator() {
        return diceCombinations.iterator();
    }

    static class DiceCombination implements Iterable<Dice>{
        private final List<Dice> diceCombinations;

        DiceCombination(List<Dice> diceCombinations) {
            this.diceCombinations = diceCombinations;
        }

        DiceCombination(Dice... diceCombinations) {
            this.diceCombinations = new ArrayList<>();
            this.diceCombinations.addAll(Arrays.asList(diceCombinations));
        }

        @Override
        public Iterator<Dice> iterator() {
            return diceCombinations.iterator();
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof DiceCombination)) return false;
            DiceCombination other = (DiceCombination) obj;

            if (this.diceCombinations.size() != other.diceCombinations.size()) return false;

            for (int it = 0; it < this.diceCombinations.size(); ++it) {
                if (!this.diceCombinations.get(it).equals(other.diceCombinations.get(it))) return false;
            }

            return true;
        }
    }
}
