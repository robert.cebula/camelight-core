package com.gitlab.robert.cebula.camelight_core;

import java.util.Map;

public class StageProbabilities {
    private final Probabilities winnersProbabilities;
    private final Probabilities secondPlacesProbabilities;
    private final int[] steps;

    StageProbabilities(Map<CamelColor, Integer> numberOfWinsByCamelColor,
                       Map<CamelColor, Integer> numberOfSecondPlaceByCamelColor,
                       int[] steps) {
        this.winnersProbabilities = new Probabilities(numberOfWinsByCamelColor);
        this.secondPlacesProbabilities = new Probabilities(numberOfSecondPlaceByCamelColor);
        this.steps = steps;
    }

    public Probabilities winnersProbabilities() {
        return winnersProbabilities;
    }

    public Probabilities secondPlacesProbabilities() {
        return secondPlacesProbabilities;
    }

    public int[] steps() {
        return steps;
    }
}
