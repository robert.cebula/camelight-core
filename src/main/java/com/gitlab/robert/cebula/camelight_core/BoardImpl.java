package com.gitlab.robert.cebula.camelight_core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardImpl implements Board, Serializable {
    static final int BOARD_SIZE = 16;
    public static final int META_POSITION = 15; // camel wins if it steps at the next field
    public static final int MAX_CAMEL_STACK = 5;
    private final BoardElement[] boardElements = new BoardElement[BOARD_SIZE];

    public static BoardImpl constructBoard(Map<Integer, BoardElement[]> boardElementsAtPosition) {
        BoardImpl board = new BoardImpl();

        for (int position : boardElementsAtPosition.keySet()) {
            BoardElement[] boardElements = boardElementsAtPosition.get(position);

            BoardElement firstBoardElement = boardElements[0];

            if (firstBoardElement instanceof Camel) {
                Camel currentCamel = (Camel)firstBoardElement;
                for (int it = 1; it < boardElements.length; ++it) {
                    Camel nextCamel = (Camel)boardElements[it];
                    currentCamel.setUpCamel(nextCamel);
                    nextCamel.setBottomCamel(currentCamel);
                    currentCamel = nextCamel;
                }
            }

            board.setBoardElement(firstBoardElement, position);
        }

        return board;
    }

    public static BoardImpl constructBoard(List<Dice> dices) {
        Map<Integer, List<BoardElement>> boardElementsAtPosition = new HashMap<>();

        for (Dice dice : dices) {
            int position = dice.value() - 1;
            if (!boardElementsAtPosition.containsKey(position)) {
                boardElementsAtPosition.put(position, new ArrayList<>());
            }

            List<BoardElement> boardElements = boardElementsAtPosition.get(position);
            boardElements.add(new Camel(dice.camelColor()));
        }

        Map<Integer, BoardElement[]> boardElementsArrayAtPosition = new HashMap<>();
        for (Integer position : boardElementsAtPosition.keySet()) {
            List<BoardElement> boardElements = boardElementsAtPosition.get(position);
            boardElementsArrayAtPosition.put(position, boardElements.toArray(new BoardElement[0]));
        }

        return constructBoard(boardElementsArrayAtPosition);
    }

    BoardImpl() {
        Arrays.fill(boardElements, new EmptyBoardElement());
    }

    public BoardImpl(BoardImpl other) {
        for (int it = 0; it < BOARD_SIZE; ++it) {
            boardElements[it] = other.boardElements[it].copy();
        }
    }

    @Override
    public boolean containsPlates() {
        for (BoardElement boardElement : boardElements) {
            if (boardElement instanceof Plate) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean removePlates() {
        boolean removed = false;
        for (int it = 0; it < BOARD_SIZE; ++it) {
            if (boardElements[it] instanceof Plate) {
                boardElements[it] = new EmptyBoardElement();
                removed = true;
            }
        }
        return removed;
    }

    @Override
    public Optional<Plate> getPlateAtPosition(int position) {
        BoardElement boardElement = boardElements[position];

        if (boardElement instanceof Plate) {
            return Optional.of((Plate)boardElement);
        }

        return Optional.empty();
    }

    @Override
    public boolean addOrSwitchPlateAtPosition(Plate newPlate, int position) {
        if (!canPlateBePlacedAtPosition(position)) {
            return false;
        }

        if (newPlate == null) {
            boardElements[position] = new EmptyBoardElement();
            return true;
        }

        boardElements[position] = newPlate;
        return true;
    }

    @Override
    public void removePlateAtPosition(int position) {
        if (boardElements[position] instanceof Plate) {
            boardElements[position] = new EmptyBoardElement();
            return;
        }

        throw new IllegalArgumentException("There is no plate at position " + position);
    }

    @Override
    public void move(Dice dice) {
        CamelWithPosition camelWithPosition = findCamel(dice.camelColor());
        Camel camel = camelWithPosition.camel();
        camel.move(dice.value(), this, camelWithPosition.position());
    }

    public Integer moveAndReturnSteppedPosition(Dice dice) {
        CamelWithPosition camelWithPosition = findCamel(dice.camelColor());
        Camel camel = camelWithPosition.camel();
        camel.move(dice.value(), this, camelWithPosition.position());

        return (camelWithPosition.position + dice.value()) % BOARD_SIZE;
    }

    @Override
    public boolean tryToMoveUpperCamel(int startingPosition, int endingPosition) {
        if (startingPosition == endingPosition) {
            return false;
        }

        BoardElement startingBoardElement = boardElements[startingPosition];
        if (!(startingBoardElement instanceof Camel)) {
            return false;
        }

        Camel camel = (Camel) startingBoardElement;
        camel = camel.mostUpperCamel();

        BoardElement endingBoardElement = boardElements[endingPosition];
        if (endingBoardElement instanceof Plate) {
            return false;
        }

        Camel bottomCamel = camel.getBottomCamel();
        if (bottomCamel != null) {
            bottomCamel.setUpCamel(null);
            camel.setBottomCamel(null);
        } else {
            removeCamelIfExist(startingPosition);
        }

        if (endingBoardElement instanceof EmptyBoardElement) {
            setBoardElement(camel, endingPosition);
        }

        if (endingBoardElement instanceof Camel) {
            Camel endingCamel = (Camel) endingBoardElement;
            endingCamel = endingCamel.mostUpperCamel();
            endingCamel.setUpCamel(camel);
            camel.setBottomCamel(endingCamel);
        }

        return true;
    }

    @Override
    public GameState currentGameState() {
        for (int it = 0; it < BOARD_SIZE; ++it) {
            BoardElement boardElement = boardElements[it];
            if (boardElement instanceof Camel) {
                Camel camel = (Camel) boardElement;
                if (camel.itOrUpperCamelsWentThroughFinish()) {
                    return GameState.END;
                }
            }
        }

        return GameState.ONGOING;
    }

    @Override
    public BoardElement getBoardElement(int position) {
        return this.boardElements[position];
    }

    private int floorMod(int x, int y) {
        return (((x % y) + y) % y);
    }

    private boolean canPlateBePlacedAtPosition(int position) {
        // we need to check if it's not starting position, there is no camel at this position and if neighbours are not
        // plates
        if (position == 0) {
            return false;
        }

        if (boardElements[position] instanceof Camel) {
            return false;
        }

        int leftNeighbour = floorMod(position - 1, META_POSITION + 1);
        int rightNeighbour = floorMod(position + 1, META_POSITION + 1);

        if (boardElements[leftNeighbour] instanceof Plate) {
            return false;
        }

        if (boardElements[rightNeighbour] instanceof Plate) {
            return false;
        }

        return true;
    }

    private CamelWithPosition findCamel(CamelColor camelColor) {
        for (int it = 0; it < BOARD_SIZE; ++it) {
            BoardElement boardElement = boardElements[it];
            if (boardElement instanceof Camel) {
                Camel camel = (Camel) boardElement;

                while (camel != null) {
                    if (camel.isColor(camelColor)) {
                        return new CamelWithPosition(camel, it);
                    }

                    camel = camel.getUpCamel();
                }
            }
        }

        throw new RuntimeException("Unable to find camel " + camelColor.toString() + " on board");
    }

    void removeCamelIfExist(int position) {
        BoardElement boardElement = this.boardElements[position];
        if (boardElement instanceof Camel) {
            this.boardElements[position] = new EmptyBoardElement();
        }
    }

    void setBoardElement(BoardElement boardElement, int position) {
        this.boardElements[position] = boardElement;
    }

    CamelResults getCamelResults() {
        return new CamelResults(boardElements);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        for (int position = 0; position < BOARD_SIZE; ++position) {
            result.append("+-").append(String.format("%-2d", position + 1)).append("-+");
        }
        result.append("\n");

        for (int it = 0; it < BOARD_SIZE * 6; ++it) {
            result.append("-");
        }
        result.append("\n");

        for (int row = 0; row < MAX_CAMEL_STACK; ++row) {
            for (int position = 0; position < BOARD_SIZE; ++position) {
                BoardElement boardElement = boardElements[position];
                if (boardElement instanceof Camel) {
                    int nextRow = 1;
                    Camel currentCamel = (Camel)boardElement;
                    while (nextRow <= row) {
                        currentCamel = currentCamel.getUpCamel();
                        if (currentCamel == null) break;
                        nextRow++;
                    }

                    if (currentCamel != null) {
                        result.append("| ").append(currentCamel.toString()).append(" |");
                    }
                    else {
                        result.append("| ").append("  ").append(" |");
                    }

                    continue;
                }

                if (row == 0) {
                    result.append("| ").append(boardElement.toString()).append(" |");
                }
                else {
                    result.append("| ").append("  ").append(" |");
                }
            }

            result.append("\n");
        }
        result.append("\n");

        for (int it = 0; it < BOARD_SIZE * 6; ++it) {
            result.append("-");
        }
        result.append("\n");

        return result.toString();
    }

    private static class CamelWithPosition {
        private final Camel camel;
        private final int position;

        CamelWithPosition(Camel camel, int position) {
            this.camel = camel;
            this.position = position;
        }

        Camel camel() {
            return camel;
        }

        int position() {
            return position;
        }
    }

    static class CamelResults {
        private final List<CamelColor> camelsPositionsAscending = new ArrayList<>();

        CamelResults(CamelColor... camelColors) {
            camelsPositionsAscending.addAll(Arrays.asList(camelColors));
        }

        CamelResults(BoardElement[] boardElements) {
            List<CamelColor> camelsThatWentThrouhtFinish = new ArrayList<>();
            List<CamelColor> camelsThatDontWentThroughFinish = new ArrayList<>();
            for (BoardElement boardElement : boardElements) {
                if (boardElement instanceof Camel) {
                    Camel camel = (Camel) boardElement;

                    while (camel != null) {
                        if (camel.wentThroughFinish()) {
                            camelsThatWentThrouhtFinish.add(camel.color());
                        } else {
                            camelsThatDontWentThroughFinish.add(camel.color());
                        }

                        camel = camel.getUpCamel();
                    }
                }
            }

            camelsPositionsAscending.addAll(camelsThatDontWentThroughFinish);
            camelsPositionsAscending.addAll(camelsThatWentThrouhtFinish);
        }

        CamelColor getWinner() {
            return camelsPositionsAscending.get(camelsPositionsAscending.size() - 1);
        }

        CamelColor getLooser() {
            return camelsPositionsAscending.get(0);
        }

        CamelColor getSecondPlace() {
            return camelsPositionsAscending.get(camelsPositionsAscending.size() - 2);
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder();

            for (CamelColor camelColor : camelsPositionsAscending) {
                result.append(camelColor.toString(), 0, 1).append("\n");
            }

            return result.toString();
        }
    }
}
