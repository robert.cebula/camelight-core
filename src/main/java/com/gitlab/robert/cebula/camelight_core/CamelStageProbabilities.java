package com.gitlab.robert.cebula.camelight_core;

import java.util.*;

import static com.gitlab.robert.cebula.camelight_core.BoardImpl.BOARD_SIZE;

public class CamelStageProbabilities {
    private final BoardImpl startingBoard;
    private final List<CamelColor> leftCamelColors;

    public CamelStageProbabilities(BoardImpl startingBoard, List<CamelColor> leftCamelColors) {
        this.startingBoard = startingBoard;
        this.leftCamelColors = leftCamelColors;
    }

    public StageProbabilities countProbabilities() {
        PossibleDiceCombinations possibleDiceCombinations = new PossibleDiceCombinations(leftCamelColors);

        Map<CamelColor, Integer> numberOfWinsByCamelColor = new HashMap<>();
        for (CamelColor camelColor : CamelColor.values()) {
            numberOfWinsByCamelColor.put(camelColor, 0);
        }

        Map<CamelColor, Integer> numberOfSecondPlacesByCamelColor = new HashMap<>();
        for (CamelColor camelColor : CamelColor.values()) {
            numberOfSecondPlacesByCamelColor.put(camelColor, 0);
        }

        int[] steps = new int[BOARD_SIZE];
        for (int it = 0; it < steps.length; ++it) {
            steps[it] = 0;
        }

        // special case when there is already camel that wins whole race
        if (startingBoard.currentGameState().equals(GameState.END)) {
            CamelColor winner = startingBoard.getCamelResults().getWinner();
            numberOfWinsByCamelColor.put(winner, 1);
            CamelColor secondPlace = startingBoard.getCamelResults().getSecondPlace();
            numberOfSecondPlacesByCamelColor.put(secondPlace, 1);
            return new StageProbabilities(numberOfWinsByCamelColor, numberOfSecondPlacesByCamelColor, steps);
        }

        for (PossibleDiceCombinations.DiceCombination diceCombination : possibleDiceCombinations) {
            BoardImpl boardAfterMove = new BoardImpl(startingBoard);

            for (Dice dice : diceCombination) {
                Integer steppedPosition = boardAfterMove.moveAndReturnSteppedPosition(dice);
                steps[steppedPosition] += 1;
                if (boardAfterMove.currentGameState().equals(GameState.END)) {
                    break;
                }
            }

            CamelColor winner = boardAfterMove.getCamelResults().getWinner();
            numberOfWinsByCamelColor.put(winner, numberOfWinsByCamelColor.get(winner) + 1);
            CamelColor secondPlace = boardAfterMove.getCamelResults().getSecondPlace();
            numberOfSecondPlacesByCamelColor.put(secondPlace, numberOfSecondPlacesByCamelColor.get(secondPlace) + 1);
        }

        return new StageProbabilities(numberOfWinsByCamelColor, numberOfSecondPlacesByCamelColor, steps);
    }
}
