package com.gitlab.robert.cebula.camelight_core;

import java.io.Serializable;

public class Dice implements Serializable {
    public static final int MIN_VALUE = 1;
    public static final int MAX_VALUE = 3;
    private final CamelColor camelColor;
    private final int value;

    public Dice(CamelColor camelColor, int value) {
        this.camelColor = camelColor;

        validateValue(value);
        this.value = value;
    }

    private void validateValue(int value) {
        assert value >= MIN_VALUE;
        assert value <= MAX_VALUE;
    }

    public CamelColor camelColor() {
        return camelColor;
    }

    public int value() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Dice)) return false;

        Dice other = (Dice) obj;

        return this.camelColor.equals(other.camelColor) && this.value == other.value;
    }
}
