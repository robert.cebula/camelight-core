package com.gitlab.robert.cebula.camelight_core;

import java.util.Objects;

public class Optional<T> {

    private T value;

    private Optional() {
        this.value = null;
    }

    private Optional(T value) {
        this.value = Objects.requireNonNull(value);
    }

    public static<T> Optional<T> empty() {
        return new Optional<>();
    }

    public static<T> Optional<T> of(T value) {
        return new Optional<>(value);
    }

    public interface Action<T> {
        void apply(T value);
    }

    public boolean isPresent() {
        return value != null;
    }

    public T orElse(T elseValue) {
        return value != null ? value : elseValue;
    }

    public T get() {
        return value;
    }

    public void ifPresent(Action<T> action) {
        if (value != null) {
            action.apply(value);
        }
    }
}
