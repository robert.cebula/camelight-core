package com.gitlab.robert.cebula.camelight_core;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class CamelRaceProbabilities {
    private final StageBoard startingBoard;
    private int numberOfIterations;

    Map<CamelColor, Integer> numberOfWinsByCamelColor;
    Map<CamelColor, Integer> numberOfLoosesByCamelColor;

    public CamelRaceProbabilities(StageBoard startingBoard) {
        this.startingBoard = new StageBoard(startingBoard);

        this.numberOfIterations = 0;

        this.numberOfWinsByCamelColor = new HashMap<>();
        for (CamelColor camelColor : CamelColor.values()) {
            numberOfWinsByCamelColor.put(camelColor, 0);
        }

        this.numberOfLoosesByCamelColor = new HashMap<>();
        for (CamelColor camelColor : CamelColor.values()) {
            numberOfLoosesByCamelColor.put(camelColor, 0);
        }
    }

    public int numberOfIterations() {
        return numberOfIterations;
    }

    public void iteration() {
        numberOfIterations++;
        StageBoard currentStageBoard = new StageBoard(this.startingBoard);

        while (true) {
            boolean removePlates = currentStageBoard.leftColors().size() == 1;
            Dice randomDice = randomDice(currentStageBoard.leftColors());
            currentStageBoard.move(randomDice);

            if (currentStageBoard.currentGameState().equals(GameState.END)) {
                CamelColor winningCamelColor = currentStageBoard.boardImpl().getCamelResults().getWinner();
                numberOfWinsByCamelColor.put(winningCamelColor, numberOfWinsByCamelColor.get(winningCamelColor) + 1);

                CamelColor loosingCamelColor = currentStageBoard.boardImpl().getCamelResults().getLooser();
                numberOfLoosesByCamelColor.put(loosingCamelColor, numberOfLoosesByCamelColor.get(loosingCamelColor) + 1);

                break;
            }

            if (removePlates) {
                currentStageBoard.removePlates();
            }
        }
    }

    private Dice randomDice(List<CamelColor> leftColors) {
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();

        CamelColor randomCamelColor = leftColors.get(threadLocalRandom.nextInt(leftColors.size()));
        int randomValue = threadLocalRandom.nextInt(Dice.MAX_VALUE) + 1;

        return new Dice(randomCamelColor, randomValue);
    }

    public RaceProbabilities getRaceProbabilities() {
        return new RaceProbabilities(numberOfWinsByCamelColor, numberOfLoosesByCamelColor);
    }
}
