package com.gitlab.robert.cebula.camelight_core;

import java.io.Serializable;
import java.util.Map;

public class RaceProbabilities implements Serializable {

    private final Probabilities winnersProbabilities;
    private final Probabilities loosersProbabilities;

    RaceProbabilities(Map<CamelColor, Integer> numberOfWinsByCamelColor, Map<CamelColor, Integer> numberOfLoosesByCamelColor) {
        this.winnersProbabilities = new Probabilities(numberOfWinsByCamelColor);
        this.loosersProbabilities = new Probabilities(numberOfLoosesByCamelColor);
    }

    public Probabilities winnersProbabilities() {
        return winnersProbabilities;
    }

    public Probabilities loosersProbabilities() {
        return loosersProbabilities;
    }
}
